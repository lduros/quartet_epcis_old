.. :changelog:

History
-------

0.1.0 (2017-12-07)
++++++++++++++++++

* First release on PyPI.

1.0.+ (2018-16-2017)
++++++++++++++++++

* First production-ready release.
* CI build to auto-deploy tags to PyPI
